$.drawer.open();

function toggle(e) {
    var fn = 'toggle' + e.source.title + 'Window';
    $.drawer[fn]();
}

// Setup the intro window
var introWindow = Alloy.createController("intro", {
	loaded_callback : function(_event) {
		introWindow.close();
	}
}).getView();

// Open the intro window
introWindow.open();